# Automating Scheduled Builds

This repo supports the article [Automate scheduled builds with Hugo, Netlify, and GitLab’s CI/CD pipeline](https://www.neotericdesign.com/articles/2020/02/automate-scheduled-builds-with-hugo-netlify-and-gitlabs-ci/cd-pipeline/).

## Install

* This demo uses Neoteric's test theme, Cosette. Clone with: `git clone --recurse-submodules https://gitlab.com/nicfits/scheduled-hugo-build-demo.git`
