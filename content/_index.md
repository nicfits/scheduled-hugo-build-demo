+++
title = ""
date = 2020-02-12T10:38:33-06:00
menu = "main"
+++
Welcome!  This is a demo Hugo site for the article [Automate scheduled builds with Hugo, Netlify, and GitLab’s CI/CD pipeline](https://www.neotericdesign.com/articles/2020/02/automate-scheduled-builds-with-hugo-netlify-and-gitlabs-ci/cd-pipeline/).

Run `hugo new <filename>.md` to generate some new test pages, and watch them sync up to Netlify on your specified schedule.
